import org.uma.jmetal.problem.DoubleProblem;
import org.uma.jmetal.problem.Problem;
import org.uma.jmetal.problem.multiobjective.UF.*;
import org.uma.jmetal.problem.multiobjective.dtlz.*;
import org.uma.jmetal.problem.multiobjective.zdt.*;
import org.uma.jmetal.solution.DoubleSolution;
import org.uma.jmetal.util.JMetalLogger;
import org.uma.jmetal.util.experiment.util.ExperimentProblem;
import org.uma.jmetal.util.fileoutput.OutputDirectory;
import org.uma.jmetal.util.fileoutput.SolutionListOutput;
import org.uma.jmetal.util.front.Front;
import org.uma.jmetal.util.front.imp.ArrayFront;
import org.uma.jmetal.util.front.util.FrontUtils;
import org.uma.jmetal.util.point.util.PointSolution;
import org.uma.jmetal.util.point.util.distance.EuclideanDistance;
import org.uma.jmetal.utility.paretoFront.ParetoFront;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) throws IOException {
        List<ExperimentProblem<DoubleSolution>> problemList = new ArrayList<>();
        problemList.add(new ExperimentProblem<>(new ZDT1()));
        problemList.add(new ExperimentProblem<>(new ZDT2()));
        problemList.add(new ExperimentProblem<>(new ZDT3()));
        problemList.add(new ExperimentProblem<>(new ZDT4()));
        problemList.add(new ExperimentProblem<>(new ZDT6()));
        problemList.add(new ExperimentProblem<>(new DTLZ1()).changeReferenceFrontTo("DTLZ1.3D.pf"));
        problemList.add(new ExperimentProblem<>(new DTLZ2()).changeReferenceFrontTo("DTLZ2.3D.pf"));
        problemList.add(new ExperimentProblem<>(new DTLZ3()).changeReferenceFrontTo("DTLZ3.3D.pf"));
        problemList.add(new ExperimentProblem<>(new DTLZ4()).changeReferenceFrontTo("DTLZ4.3D.pf"));
        problemList.add(new ExperimentProblem<>(new DTLZ5()).changeReferenceFrontTo("DTLZ5.3D.pf"));
        problemList.add(new ExperimentProblem<>(new DTLZ6()).changeReferenceFrontTo("DTLZ6.3D.pf"));
        problemList.add(new ExperimentProblem<>(new DTLZ7()).changeReferenceFrontTo("DTLZ7.3D.pf"));
//        problemList.add(new ExperimentProblem<>(new LZ09F1()).changeReferenceFrontTo("LZ09_F1.pf"));
//        problemList.add(new ExperimentProblem<>(new LZ09F2()).changeReferenceFrontTo("LZ09_F2.pf"));
//        problemList.add(new ExperimentProblem<>(new LZ09F3()).changeReferenceFrontTo("LZ09_F3.pf"));
//        problemList.add(new ExperimentProblem<>(new LZ09F4()).changeReferenceFrontTo("LZ09_F4.pf"));
//        problemList.add(new ExperimentProblem<>(new LZ09F5()).changeReferenceFrontTo("LZ09_F5.pf"));
//        problemList.add(new ExperimentProblem<>(new LZ09F6()).changeReferenceFrontTo("LZ09_F6.pf"));
//        problemList.add(new ExperimentProblem<>(new LZ09F7()).changeReferenceFrontTo("LZ09_F7.pf"));
//        problemList.add(new ExperimentProblem<>(new LZ09F8()).changeReferenceFrontTo("LZ09_F8.pf"));
//        problemList.add(new ExperimentProblem<>(new LZ09F9()).changeReferenceFrontTo("LZ09_F9.pf"));
        problemList.add(new ExperimentProblem<>(new UF1()));
        problemList.add(new ExperimentProblem<>(new UF2()));
        problemList.add(new ExperimentProblem<>(new UF3()));
        problemList.add(new ExperimentProblem<>(new UF4()));
        problemList.add(new ExperimentProblem<>(new UF5()));
        problemList.add(new ExperimentProblem<>(new UF6()));
        problemList.add(new ExperimentProblem<>(new UF7()));
        problemList.add(new ExperimentProblem<>(new UF8()));
        problemList.add(new ExperimentProblem<>(new UF9()));
        problemList.add(new ExperimentProblem<>(new UF10()));

        String experimentBaseDirectory = "pf/";
        OutputDirectory.prepare(experimentBaseDirectory);

        for (int i = 0; i < problemList.size(); i++) {
            DoubleProblem problem = (DoubleProblem) problemList.get(i).getProblem();
            Front front = new ParetoFront(problem).getParetoFront();
            List<PointSolution> population = FrontUtils.convertFrontToSolutionList(front);
            new SolutionListOutput(population)
                    .setSeparator("\t")
                    .printObjectivesToFile(experimentBaseDirectory + problem.getName() + ".pf");
        }

        for (int i = 0; i < problemList.size(); i++) {
            String jmetalReferenceFrontName = experimentBaseDirectory + problemList.get(i).getProblem().getName() + ".pf";
            Front jmetalReferenceFront = new ArrayFront(jmetalReferenceFrontName);

            String platEMOReferenceFrontName = "pf_generator - II/pf/" + problemList.get(i).getProblem().getName() + ".pf";
            Front platEMOReferenceFront = new ArrayFront(platEMOReferenceFrontName);

            boolean flag = verification(jmetalReferenceFront, platEMOReferenceFront, problemList.get(i).getProblem());
        }

    }

    public static boolean verification(Front front1, Front front2, Problem problem) {
       boolean flag = true;

        if (front1.getNumberOfPoints() != front2.getNumberOfPoints()) {
            JMetalLogger.logger.warning(problem.getName() + " ==> 个数不一致 | " + front1.getNumberOfPoints() + " <--> " + front2.getNumberOfPoints());
            return false;
        }

        if (front1.getPointDimensions() != front2.getPointDimensions()) {
            JMetalLogger.logger.warning(problem.getName() + " ==> 维度不一致 | " + front1.getPointDimensions() + " <--> " + front2.getPointDimensions());
            return false;
        }

        EuclideanDistance distance = new EuclideanDistance();
        double sum = 0.0;
        for (int i = 0; i < front1.getNumberOfPoints(); i++) {
            sum += distance.compute(front1.getPoint(i), front2.getPoint(i));
        }

        if (sum / front1.getNumberOfPoints() > 1.0e-4) {
            JMetalLogger.logger.warning(problem.getName() + " ==> 数值不一致");
            return false;
        }

       return flag;
    }
}
