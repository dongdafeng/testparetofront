function pf = get_pf(problem, M, D, size)


    %% UF1-UF10
    if(strcmp(problem,'UF1') == 1)
        pf = pf_generator({'-problem',@UF1}, size);
    elseif(strcmp(problem,'UF2') == 1)
        pf = pf_generator({'-problem',@UF2}, size);
    elseif(strcmp(problem,'UF3') == 1)
        pf = pf_generator({'-problem',@UF3}, size);
    elseif(strcmp(problem,'UF4') == 1)
        pf = pf_generator({'-problem',@UF4}, size);
    elseif(strcmp(problem,'UF5') == 1)
        pf = pf_generator({'-problem',@UF5}, size);
    elseif(strcmp(problem,'UF6') == 1)
        pf = pf_generator({'-problem',@UF6}, size);
    elseif(strcmp(problem,'UF7') == 1)
        pf = pf_generator({'-problem',@UF7}, size);
    elseif(strcmp(problem,'UF8') == 1)
        pf = pf_generator({'-problem',@UF8}, size);
    elseif(strcmp(problem,'UF9') == 1)
        pf = pf_generator({'-problem',@UF9}, size);
    elseif(strcmp(problem,'UF10') == 1)
        pf = pf_generator({'-problem',@UF10}, size);
    elseif(strcmp(problem,'UF11') == 1)
        pf = pf_generator({'-problem',@UF11}, size);
    elseif(strcmp(problem,'UF12') == 1)
        pf = pf_generator({'-problem',@UF12}, size);

    %% CF1-CF10
    elseif(strcmp(problem,'CF1') == 1)
        pf = pf_generator({'-problem',@CF1}, size);
    elseif(strcmp(problem,'CF2') == 1)
        pf = pf_generator({'-problem',@CF2}, size);
    elseif(strcmp(problem,'CF3') == 1)
        pf = pf_generator({'-problem',@CF3}, size);
    elseif(strcmp(problem,'CF4') == 1)
        pf = pf_generator({'-problem',@CF4}, size);
    elseif(strcmp(problem,'CF5') == 1)
        pf = pf_generator({'-problem',@CF5}, size);
    elseif(strcmp(problem,'CF6') == 1)
        pf = pf_generator({'-problem',@CF6}, size);
    elseif(strcmp(problem,'CF7') == 1)
        pf = pf_generator({'-problem',@CF7}, size);
    elseif(strcmp(problem,'CF8') == 1)
        pf = pf_generator({'-problem',@CF8}, size);
    elseif(strcmp(problem,'CF9') == 1)
        pf = pf_generator({'-problem',@CF9}, size);
    elseif(strcmp(problem,'CF10') == 1)
        pf = pf_generator({'-problem',@CF10}, size);

    %% DTLZ1-DTLZ9
    elseif(strcmp(problem,'DTLZ1') == 1)
        pf = pf_generator({'-problem',@DTLZ1,'-M',M}, size);
    elseif(strcmp(problem,'DTLZ2') == 1)
        pf = pf_generator({'-problem',@DTLZ2,'-M',M}, size);
    elseif(strcmp(problem,'DTLZ3') == 1)
        pf = pf_generator({'-problem',@DTLZ3,'-M',M}, size);
    elseif(strcmp(problem,'DTLZ4') == 1)
        pf = pf_generator({'-problem',@DTLZ4,'-M',M}, size);
    elseif(strcmp(problem,'DTLZ5') == 1)
        pf = pf_generator({'-problem',@DTLZ5,'-M',M}, size);
    elseif(strcmp(problem,'DTLZ6') == 1)
        pf = pf_generator({'-problem',@DTLZ6,'-M',M}, size);
    elseif(strcmp(problem,'DTLZ7') == 1)
        pf = pf_generator({'-problem',@DTLZ7,'-M',M}, size);
    elseif(strcmp(problem,'DTLZ8') == 1)
        pf = pf_generator({'-problem',@DTLZ8,'-M',M}, size);
    elseif(strcmp(problem,'DTLZ9') == 1)
        pf = pf_generator({'-problem',@DTLZ9,'-M',M}, size);

    %% C1_DTLZ1, C2_DTLZ2, C3_DTLZ4, CDTLZ2
    elseif(strcmp(problem,'C1_DTLZ1') == 1)
        pf = pf_generator({'-problem',@C1_DTLZ1,'-M',M}, size);
    elseif(strcmp(problem,'C2_DTLZ2') == 1)
        pf = pf_generator({'-problem',@C2_DTLZ2,'-M',M}, size);
    elseif(strcmp(problem,'C3_DTLZ4') == 1)
        pf = pf_generator({'-problem',@C3_DTLZ4,'-M',M}, size);

    %% IDTLZ1, IDTLZ2
    elseif(strcmp(problem,'IDTLZ1') == 1)
        pf = pf_generator({'-problem',@IDTLZ1,'-M',M}, size);
    elseif(strcmp(problem,'IDTLZ2') == 1)
        pf = pf_generator({'-problem',@IDTLZ2,'-M',M}, size);

    %% WFG1-WFG9
    elseif(strcmp(problem,'WFG1') == 1)
        pf = pf_generator({'-problem',@WFG1,'-M',M}, size);
    elseif(strcmp(problem,'WFG2') == 1)
        pf = pf_generator({'-problem',@WFG2,'-M',M}, size);
    elseif(strcmp(problem,'WFG3') == 1)
        pf = pf_generator({'-problem',@WFG3,'-M',M}, size);
    elseif(strcmp(problem,'WFG4') == 1)
        pf = pf_generator({'-problem',@WFG4,'-M',M}, size);
    elseif(strcmp(problem,'WFG5') == 1)
        pf = pf_generator({'-problem',@WFG5,'-M',M}, size);
    elseif(strcmp(problem,'WFG6') == 1)
        pf = pf_generator({'-problem',@WFG6,'-M',M}, size);
    elseif(strcmp(problem,'WFG7') == 1)
        pf = pf_generator({'-problem',@WFG7,'-M',M}, size);
    elseif(strcmp(problem,'WFG8') == 1)
        pf = pf_generator({'-problem',@WFG8,'-M',M}, size);
    elseif(strcmp(problem,'WFG9') == 1)
        pf = pf_generator({'-problem',@WFG9,'-M',M}, size);

    %% ZTD1-ZTD6
    elseif(strcmp(problem,'ZDT1') == 1)
        pf = pf_generator({'-problem',@ZDT1}, size);
    elseif(strcmp(problem,'ZDT2') == 1)
        pf = pf_generator({'-problem',@ZDT2}, size);
    elseif(strcmp(problem,'ZDT3') == 1)
        pf = pf_generator({'-problem',@ZDT3}, size);
    elseif(strcmp(problem,'ZDT4') == 1)
        pf = pf_generator({'-problem',@ZDT4}, size);
    elseif(strcmp(problem,'ZDT5') == 1)
        pf = pf_generator({'-problem',@ZDT5}, size);
    elseif(strcmp(problem,'ZDT6') == 1)
        pf = pf_generator({'-problem',@ZDT6}, size);

    %% MaF1-MaF15
    elseif(strcmp(problem,'MaF1') == 1)
        pf = pf_generator({'-problem',@MaF1,'-M',M}, size);
    elseif(strcmp(problem,'MaF2') == 1)
        pf = pf_generator({'-problem',@MaF2,'-M',M}, size);
    elseif(strcmp(problem,'MaF3') == 1)
        pf = pf_generator({'-problem',@MaF3,'-M',M}, size);
    elseif(strcmp(problem,'MaF4') == 1)
        pf = pf_generator({'-problem',@MaF4,'-M',M}, size);
    elseif(strcmp(problem,'MaF5') == 1)
        pf = pf_generator({'-problem',@MaF5,'-M',M}, size);
    elseif(strcmp(problem,'MaF6') == 1)
        pf = pf_generator({'-problem',@MaF6,'-M',M}, size);
    elseif(strcmp(problem,'MaF7') == 1)
        pf = pf_generator({'-problem',@MaF7,'-M',M}, size);
    elseif(strcmp(problem,'MaF8') == 1)
        pf = pf_generator({'-problem',@MaF8,'-M',M}, size);
    elseif(strcmp(problem,'MaF9') == 1)
        pf = pf_generator({'-problem',@MaF9,'-M',M}, size);
    elseif(strcmp(problem,'MaF10') == 1)
        pf = pf_generator({'-problem',@MaF10,'-M',M}, size);
    elseif(strcmp(problem,'MaF11') == 1)
        pf = pf_generator({'-problem',@MaF11,'-M',M}, size);
    elseif(strcmp(problem,'MaF12') == 1)
        pf = pf_generator({'-problem',@MaF12,'-M',M}, size);
    elseif(strcmp(problem,'MaF13') == 1)
        pf = pf_generator({'-problem',@MaF13,'-M',M}, size);
    elseif(strcmp(problem,'MaF14') == 1)
        pf = pf_generator({'-problem',@MaF14,'-M',M}, size);
    elseif(strcmp(problem,'MaF15') == 1)
        pf = pf_generator({'-problem',@MaF15,'-M',M}, size);

     %% LZ09F1-LZ09F9
    elseif(strcmp(problem,'LZO9F1') == 1)
        pf = pf_generator({'-problem',@MOEADDE_F1}, size);
    elseif(strcmp(problem,'LZO9F2') == 1)
        pf = pf_generator({'-problem',@MOEADDE_F2}, size);
    elseif(strcmp(problem,'LZO9F3') == 1)
        pf = pf_generator({'-problem',@MOEADDE_F3}, size);
    elseif(strcmp(problem,'LZO9F4') == 1)
        pf = pf_generator({'-problem',@MOEADDE_F4}, size);
    elseif(strcmp(problem,'LZO9F5') == 1)
        pf = pf_generator({'-problem',@MOEADDE_F5}, size);
    elseif(strcmp(problem,'LZO9F6') == 1)
        pf = pf_generator({'-problem',@MOEADDE_F6}, size);
    elseif(strcmp(problem,'LZO9F7') == 1)
        pf = pf_generator({'-problem',@MOEADDE_F7}, size);
    elseif(strcmp(problem,'LZO9F8') == 1)
        pf = pf_generator({'-problem',@MOEADDE_F8}, size);
    elseif(strcmp(problem,'LZO9F9') == 1)
        pf = pf_generator({'-problem',@MOEADDE_F9}, size);

    %% MOP1-MOP7
    elseif(strcmp(problem,'MOP1') == 1)
        pf = pf_generator({'-problem',@MOEADDE_F1}, size);
    elseif(strcmp(problem,'MOP') == 1)
        pf = pf_generator({'-problem',@MOEADDE_F2}, size);
    elseif(strcmp(problem,'MOP3') == 1)
        pf = pf_generator({'-problem',@MOEADDE_F3}, size);
    elseif(strcmp(problem,'MOP4') == 1)
        pf = pf_generator({'-problem',@MOEADDE_F4}, size);
    elseif(strcmp(problem,'MOP5') == 1)
        pf = pf_generator({'-problem',@MOEADDE_F5}, size);
    elseif(strcmp(problem,'MOP6') == 1)
        pf = pf_generator({'-problem',@MOEADDE_F6}, size);
    elseif(strcmp(problem,'MOP7') == 1)
        pf = pf_generator({'-problem',@MOEADDE_F7}, size);

    %% BT1-BT9
    elseif(strcmp(problem,'BT1') == 1)
        pf = pf_generator({'-problem',@BT1}, size);
    elseif(strcmp(problem,'BT2') == 1)
        pf = pf_generator({'-problem',@BT2}, size);
    elseif(strcmp(problem,'BT3') == 1)
        pf = pf_generator({'-problem',@BT3}, size);
    elseif(strcmp(problem,'BT4') == 1)
        pf = pf_generator({'-problem',@BT4}, size);
    elseif(strcmp(problem,'BT5') == 1)
        pf = pf_generator({'-problem',@BT5}, size);
    elseif(strcmp(problem,'BT6') == 1)
        pf = pf_generator({'-problem',@BT6}, size);
    elseif(strcmp(problem,'BT7') == 1)
        pf = pf_generator({'-problem',@BT7}, size);
    elseif(strcmp(problem,'BT8') == 1)
        pf = pf_generator({'-problem',@BT8}, size);
    elseif(strcmp(problem,'BT9') == 1)
        pf = pf_generator({'-problem',@BT9}, size);
    elseif(strcmp(problem,'BT10') == 1)
        pf = pf_generator({'-problem',@BT10}, size);

    %% LSMOP1-LSMOP9
    elseif(strcmp(problem,'LSMOP1') == 1)
        pf = pf_generator({'-problem',@LSMOP1,'-M',M}, size);
    elseif(strcmp(problem,'LSMOP2') == 1)
        pf = pf_generator({'-problem',@LSMOP2,'-M',M}, size);
    elseif(strcmp(problem,'LSMOP3') == 1)
        pf = pf_generator({'-problem',@LSMOP3,'-M',M}, size);
    elseif(strcmp(problem,'LSMOP4') == 1)
        pf = pf_generator({'-problem',@LSMOP4,'-M',M}, size);
    elseif(strcmp(problem,'LSMOP5') == 1)
        pf = pf_generator({'-problem',@LSMOP5,'-M',M}, size);
    elseif(strcmp(problem,'LSMOP6') == 1)
        pf = pf_generator({'-problem',@LSMOP6,'-M',M}, size);
    elseif(strcmp(problem,'LSMOP7') == 1)
        pf = pf_generator({'-problem',@LSMOP7,'-M',M}, size);
    elseif(strcmp(problem,'LSMOP8') == 1)
        pf = pf_generator({'-problem',@LSMOP8,'-M',M}, size);
    elseif(strcmp(problem,'LSMOP9') == 1)
        pf = pf_generator({'-problem',@LSMOP9,'-M',M}, size);

    %% RMMEDA_F1-RMMEDA_F10
    elseif(strcmp(problem,'RMMEDA_F1') == 1)
        pf = pf_generator({'-problem',@RMMEDA_F1}, size);
    elseif(strcmp(problem,'RMMEDA_F2') == 1)
        pf = pf_generator({'-problem',@RMMEDA_F2}, size);
    elseif(strcmp(problem,'RMMEDA_F3') == 1)
        pf = pf_generator({'-problem',@RMMEDA_F3}, size);
    elseif(strcmp(problem,'RMMEDA_F4') == 1)
        pf = pf_generator({'-problem',@RMMEDA_F4}, size);
    elseif(strcmp(problem,'RMMEDA_F5') == 1)
        pf = pf_generator({'-problem',@RMMEDA_F5}, size);
    elseif(strcmp(problem,'RMMEDA_F6') == 1)
        pf = pf_generator({'-problem',@RMMEDA_F6}, size);
    elseif(strcmp(problem,'RMMEDA_F7') == 1)
        pf = pf_generator({'-problem',@RMMEDA_F7}, size);
    elseif(strcmp(problem,'RMMEDA_F8') == 1)
        pf = pf_generator({'-problem',@RMMEDA_F8}, size);
    elseif(strcmp(problem,'RMMEDA_F9') == 1)
        pf = pf_generator({'-problem',@RMMEDA_F9}, size);
    elseif(strcmp(problem,'RMMEDA_F10') == 1)
        pf = pf_generator({'-problem',@RMMEDA_F10}, size);

    %% IMMOEA_F1-IMMOEA_F10
    elseif(strcmp(problem,'IMMOEA_F1') == 1)
        pf = pf_generator({'-problem',@IMMOEA_F1}, size);
    elseif(strcmp(problem,'IMMOEA_F2') == 1)
        pf = pf_generator({'-problem',@IMMOEA_F2}, size);
    elseif(strcmp(problem,'IMMOEA_F3') == 1)
        pf = pf_generator({'-problem',@IMMOEA_F3}, size);
    elseif(strcmp(problem,'IMMOEA_F4') == 1)
        pf = pf_generator({'-problem',@IMMOEA_F4}, size);
    elseif(strcmp(problem,'IMMOEA_F5') == 1)
        pf = pf_generator({'-problem',@IMMOEA_F5}, size);
    elseif(strcmp(problem,'IMMOEA_F6') == 1)
        pf = pf_generator({'-problem',@IMMOEA_F6}, size);
    elseif(strcmp(problem,'IMMOEA_F7') == 1)
        pf = pf_generator({'-problem',@IMMOEA_F7}, size);
    elseif(strcmp(problem,'IMMOEA_F8') == 1)
        pf = pf_generator({'-problem',@IMMOEA_F8}, size);
    elseif(strcmp(problem,'IMMOEA_F9') == 1)
        pf = pf_generator({'-problem',@IMMOEA_F9}, size);
    elseif(strcmp(problem,'IMMOEA_F10') == 1)
        pf = pf_generator({'-problem',@IMMOEA_F10}, size);

    %% MOTSP
    elseif(strcmp(problem,'MOTSP') == 1)
        pf = pf_generator({'-problem',@MOTSP,'-M',M,'-D',D}, size);
    %% mQAP
    elseif(strcmp(problem,'mQAP') == 1)
        pf = pf_generator({'-problem',@mQAP,'-M',M,'-D',D}, size);
    %% MOTSP
    elseif(strcmp(problem,'MOTSP') == 1)
        pf = pf_generator({'-problem',@MOTSP,'-M',M,'-D',D}, size);
        
    %% else
    else
        pf = [];
    end

end