//
// MATLAB Compiler: 6.3 (R2016b)
// Date: Fri May 05 11:33:46 2017
// Arguments: "-B" "macro_default" "-W" "cpplib:get_pf" "-T" "link:lib" "-d"
// "E:\实验室\PhD\多目标\工具箱\pf_generator - II\get_pf\for_testing" "-v"
// "E:\实验室\PhD\多目标\工具箱\pf_generator - II\get_pf.m" 
//

#ifndef __get_pf_h
#define __get_pf_h 1

#if defined(__cplusplus) && !defined(mclmcrrt_h) && defined(__linux__)
#  pragma implementation "mclmcrrt.h"
#endif
#include "mclmcrrt.h"
#include "mclcppclass.h"
#ifdef __cplusplus
extern "C" {
#endif

#if defined(__SUNPRO_CC)
/* Solaris shared libraries use __global, rather than mapfiles
 * to define the API exported from a shared library. __global is
 * only necessary when building the library -- files including
 * this header file to use the library do not need the __global
 * declaration; hence the EXPORTING_<library> logic.
 */

#ifdef EXPORTING_get_pf
#define PUBLIC_get_pf_C_API __global
#else
#define PUBLIC_get_pf_C_API /* No import statement needed. */
#endif

#define LIB_get_pf_C_API PUBLIC_get_pf_C_API

#elif defined(_HPUX_SOURCE)

#ifdef EXPORTING_get_pf
#define PUBLIC_get_pf_C_API __declspec(dllexport)
#else
#define PUBLIC_get_pf_C_API __declspec(dllimport)
#endif

#define LIB_get_pf_C_API PUBLIC_get_pf_C_API


#else

#define LIB_get_pf_C_API

#endif

/* This symbol is defined in shared libraries. Define it here
 * (to nothing) in case this isn't a shared library. 
 */
#ifndef LIB_get_pf_C_API 
#define LIB_get_pf_C_API /* No special import/export declaration */
#endif

extern LIB_get_pf_C_API 
bool MW_CALL_CONV get_pfInitializeWithHandlers(
       mclOutputHandlerFcn error_handler, 
       mclOutputHandlerFcn print_handler);

extern LIB_get_pf_C_API 
bool MW_CALL_CONV get_pfInitialize(void);

extern LIB_get_pf_C_API 
void MW_CALL_CONV get_pfTerminate(void);



extern LIB_get_pf_C_API 
void MW_CALL_CONV get_pfPrintStackTrace(void);

extern LIB_get_pf_C_API 
bool MW_CALL_CONV mlxGet_pf(int nlhs, mxArray *plhs[], int nrhs, mxArray *prhs[]);


#ifdef __cplusplus
}
#endif

#ifdef __cplusplus

/* On Windows, use __declspec to control the exported API */
#if defined(_MSC_VER) || defined(__BORLANDC__)

#ifdef EXPORTING_get_pf
#define PUBLIC_get_pf_CPP_API __declspec(dllexport)
#else
#define PUBLIC_get_pf_CPP_API __declspec(dllimport)
#endif

#define LIB_get_pf_CPP_API PUBLIC_get_pf_CPP_API

#else

#if !defined(LIB_get_pf_CPP_API)
#if defined(LIB_get_pf_C_API)
#define LIB_get_pf_CPP_API LIB_get_pf_C_API
#else
#define LIB_get_pf_CPP_API /* empty! */ 
#endif
#endif

#endif

extern LIB_get_pf_CPP_API void MW_CALL_CONV get_pf(int nargout, mwArray& pf, const mwArray& problem, const mwArray& M, const mwArray& D, const mwArray& size);

#endif
#endif
