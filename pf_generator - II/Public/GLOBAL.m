classdef GLOBAL < handle

    properties
        N;                              % Population size
        M;                              % Number of objectives
        D;                              % Number of variables
        lower;                          % Lower bound of each decision variable
        upper;                          % Upper bound of each decision variable
        evaluation = 10000;             % Maximum number of evaluations
        operator   = @EAreal;         	% Operator function
%     end
%     properties(SetAccess = private)
        parameter  = struct();      	% Parameters of functions specified by users
        problem;                        % Problem function
        PF;                             % True Pareto front
    end
   
    methods
       %% Constructor
        function obj = GLOBAL(varargin)       
            obj.Set();
            proStr = {'problem','N','M','D','operator','evaluation'};
            if nargin > 0
                % The parameter setting of the environment
                IsString = find(cellfun(@ischar,varargin(1:end-1)));
                [~,Loc]  = ismember(varargin(IsString),cellfun(@(S)['-',S],proStr,'UniformOutput',false));
                for i = find(Loc)
                    obj.(varargin{IsString(i)}(2:end)) = varargin{IsString(i)+1};
                end
                % The parameter setting of the algorithm, problem and
                % operator
                MatchString = regexp(varargin(IsString),'^\-.+_parameter$');
                Loc         = cell2mat(cellfun(@(S)~isempty(S),MatchString,'UniformOutput',false));
                for i = find(Loc)
                    obj.parameter.(varargin{IsString(i)}(2:end-10)) = varargin{IsString(i)+1};
                end
            end
            % Initialise other parameters via the test problem
            obj.Set(false);
            obj.problem('init',obj,1);     
        end
        
      %% Obtain the parameter settings from user
        function varargout = ParameterSet(obj,varargin)
        %ParameterSet - Obtain the parameter setting from user
        %
        %   [p1,p2,...] = obj.ParameterSet(v1,v2,...) returns the setting
        %   values of p1, p2, ..., where v1, v2, ... are their default
        %   values. The setting values are specified by the user with the
        %   following form:
        %   MOEA(...,'-X_parameter',{p1,p2,...},...), where X is the
        %   function name of the caller.
        %
        %   MOEA(...,'-X_parameter',{[],p2,...},...) means that parameter
        %   p1 is not specified by the user, so that it equals to its
        %   default value v1.
        %
        %   Example:
        %       [p1,p2,p3] = obj.ParameterSet(1,2,3)

            CallStack = dbstack();
            caller    = CallStack(2).name;
            varargout = varargin;
            if isfield(obj.parameter,caller)
                specified = cellfun(@(S)~isempty(S),obj.parameter.(caller));
                varargout(specified) = obj.parameter.(caller)(specified);
            end
        end
    end
    
    methods(Access = private)
        %% Access the setting of public properties
        function value = Set(obj,property,value,varargin)
            persistent lock
            switch nargin
                case 1
                    lock = struct('lock',true);
                case 2
                    lock.lock = property;
                otherwise
                    if isempty(value)
                        value = obj.(property);
                    else
                        if isfield(lock,property) && (lock.(property)||lock.lock)
                            value = obj.(property);
                        else
                            obj.Validation(value,varargin{:});
                        end
                        lock.(property) = lock.lock;
                    end
            end
        end
    end
end