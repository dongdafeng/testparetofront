function pf = pf_generator(varargin, size)

    cd(fileparts(mfilename('fullpath')));
    addpath(genpath(cd));

    obj = GLOBAL(varargin{:});
    pf = obj.problem('PF',obj,size);
end