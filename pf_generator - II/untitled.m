
problems={
%     'UF1','UF2','UF3','UF4','UF5','UF6','UF7','UF8','UF9','UF10',...
%     'ZDT1','ZDT2','ZDT3','ZDT4','ZDT6',...
    'DTLZ1','DTLZ2','DTLZ3','DTLZ4','DTLZ5','DTLZ6','DTLZ7',}

format long

for i=1:length(problems)
    problem = problems{i}
    pf = get_pf(problem,get_m(problem),0,2000);
    
    fid=fopen(['pf/' problem '.pf'],'wt');
    [m,n]=size(pf);
    for ii=1:1:m
        for jj=1:1:n
            fprintf(fid,'%g\t',pf(ii,jj));
        end
        fprintf(fid,'\n');
    end
    
end

function m = get_m(name)
    m=2;

    if strcmp(name, 'DTLZ1') == 0 || strcmp(name, 'DTLZ2') == 0 || strcmp(name, 'DTLZ3') == 0 || strcmp(name, 'DTLZ4') == 0 || strcmp(name, 'DTLZ5') == 0 || strcmp(name, 'DTLZ6') == 0 || strcmp(name, 'DTLZ7') == 0 || ...
            strcmp(name, 'UF8') == 0 || strcmp(name, 'UF9') == 0 || strcmp(name, 'UF10') == 0 || strcmp(name, 'LZ09_F6') == 0
        m=3;
    end
end

